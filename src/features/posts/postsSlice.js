import { createSlice, nanoid } from '@reduxjs/toolkit';

export const postsSlice = createSlice({
  name: 'posts',
  initialState:
   [
      {
        id: 1,
        fullName: "Nguyen Van Khan",
        email: "khannguyenvan23@gmail.com",
        phone: "0988099988",
        type: "VIP"
      },
      {
        id: 2,
        fullName: "Harry Potter",
        email: "harrypotter222@gmail.com",
        phone: "08888766666",
        type: "MEMBER"
      },
      {
        id: 3,
        fullName: "John Snow",
        email: "johnsnow@gmail.com",
        phone: "0222266666",
        type: "VIP"
      },
    ],
  reducers: {
    postAdded: {
      reducer(state, action) {
        state.push(action.payload)
      },
      prepare(fullName, email, phone, type) {
        return {
          payload: {
            id: nanoid(),
            fullName,
            email,
            phone,
            type
          }
        }
      }
    },
    postDelete(state, action) {
      if(action.payload){
        state.splice(
          state.findIndex((post)=>post.id ===action.payload),
          1
          )
      }
    },
    postUpdate(state, action) {
     const {id, fullName, email, phone, type} = action.payload
     const existingPost = state.find(post=>post.id === id)
     if (existingPost) {
       existingPost.fullName = fullName
       existingPost.email = email
       existingPost.phone = phone
       existingPost.type = type
     }
    },
  },
});


export const { postAdded, postDelete, postUpdate } = postsSlice.actions
export default postsSlice.reducer;
