import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { postAdded } from './postsSlice'
import { isShowAdd } from '../show/showAddSlice'
import { Button } from 'react-bootstrap'
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import CloseIcon from '@material-ui/icons/Close';
export default function AddPostForm(){
    const [ fullName, setFullName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ phone, setPhone ] = useState('');
    const [ type, setType ] = useState('');
    const dispatch = useDispatch()

    const handleFullNameChanged = (e) => setFullName(e.target.value)
    const handleEmailChanged = (e) => setEmail(e.target.value)
    const handlePhoneChanged = (e) => setPhone(e.target.value)
    const handleTypeChanged = (e) => setType(e.target.value)

    const canSave = Boolean(fullName) && Boolean(email) && Boolean(phone) && Boolean(type)
    const onSavePostClicked = () => {
        dispatch(postAdded(fullName, email, phone, type))
        setFullName('')
        setEmail('')
        setPhone('')
        setType('')
        dispatch(isShowAdd())
    }

    const nextPostsNumber = useSelector(state=>state.posts).length + 1
    const typeOptions = ['VIP', 'MEMBER'].map(
        (nameType, index) => (
            <option key={nameType + index}
            value={nameType}>
                {nameType}
            </option>
        ))

    return (
        <tr>

                <td>{ nextPostsNumber }</td>
                <td>
                    <input type="text"
                       value={fullName}
                        onChange={handleFullNameChanged}
                       />
                </td>
                <td>
                    <input type="text"
                        value={email}
                            onChange={handleEmailChanged}
                        />
                </td>
                <td>
                    <input type="text"
                        value={phone}
                            onChange={handlePhoneChanged}
                        />
                </td>
                <td>
                    <select value={type}
                        onChange={handleTypeChanged}>
                        <option value="" disabled></option>
                        {typeOptions}
                     </select>
                </td>
                <td>
                <Button variant="outline-primary mx-2"
                    onClick={onSavePostClicked}
                    disabled={!canSave}><PlaylistAddIcon style={{ color: "orange"}} /></Button>
                <Button variant="outline-primary"
                    onClick={()=>{dispatch(isShowAdd())}}><CloseIcon style={{ color: "red"}} /></Button>
                </td>

        </tr>

    )
}