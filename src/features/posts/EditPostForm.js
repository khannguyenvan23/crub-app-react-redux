import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { postUpdate } from './postsSlice'
import { Button } from 'react-bootstrap'
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
export default function EditPostForm({ id, handleIsShowEdit }){
    const dispatch = useDispatch()
    const post = useSelector(state=>state.posts).find((post=>post.id === id))
    const stt = useSelector(state=>state.posts).findIndex((post=>post.id === id)) + 1

    const [ fullName, setFullName ] = useState(post.fullName);
    const [ email, setEmail ] = useState(post.email);
    const [ phone, setPhone ] = useState(post.phone);
    const [ type, setType ] = useState(post.type);
    const handleFullNameChanged = (e) => setFullName(e.target.value)
    const handleEmailChanged = (e) => setEmail(e.target.value)
    const handlePhoneChanged = (e) => setPhone(e.target.value)
    const handleTypeChanged = (e) => setType(e.target.value)

    const canSave = Boolean(fullName) && Boolean(email) && Boolean(phone) && Boolean(type)
    const onSavePostClicked = () => {
        dispatch(postUpdate({id ,fullName, email, phone, type}))
        setFullName('')
        setEmail('')
        setPhone('')
        setType('')
        handleIsShowEdit()
    }


    const typeOptions = ['VIP', 'MEMBER'].map(
        (nameType, index) => (
            <option key={nameType + index}
            value={nameType}>
                {nameType}
            </option>
        ))

    return (
        <tr>

                <td>{ stt }</td>
                <td>
                    <input type="text"
                       value={fullName}
                        onChange={handleFullNameChanged}
                       />
                </td>
                <td>
                    <input type="text"
                        value={email}
                            onChange={handleEmailChanged}
                        />
                </td>
                <td>
                    <input type="text"
                        value={phone}
                            onChange={handlePhoneChanged}
                        />
                </td>
                <td>
                    <select value={type}
                        onChange={handleTypeChanged}>
                        <option value="" disabled></option>
                        {typeOptions}
                     </select>
                </td>
                <td>
                <Button variant="outline-primary mx-2"
                    onClick={onSavePostClicked}
                    disabled={!canSave}><SaveIcon style={{ color: "orange"}} /></Button>
                <Button variant="outline-primary"
                    onClick={()=>{handleIsShowEdit()}}><CancelIcon style={{ color: "red"}} /></Button>
                </td>

        </tr>

    )
}