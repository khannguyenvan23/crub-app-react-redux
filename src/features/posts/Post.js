import React, { useState } from 'react';
import { useDispatch } from 'react-redux'
import { postDelete } from './postsSlice'
import EditPostForm  from './EditPostForm'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { Button } from 'react-bootstrap'

export const Post =({post, index})=> {
  const [ showEdit, isShowEdit ]= useState(false)
  const { fullName, email, id, type, phone} = post
  const stt = index+1;
  const dispatch = useDispatch();
  const handleType = (type === "VIP" ? <b style={{color: "red"}}> + {type} + </b>: type)
  const handleIsShowEdit =()=>{
    isShowEdit(!showEdit)
  }
  if(showEdit){
    return (
      <EditPostForm id= {id} handleIsShowEdit={handleIsShowEdit} />
      )
  }
  return (
    <tr>
        <td>{ stt }</td>
        <td>{ fullName }</td>
        <td>{ email }</td>
        <td>{ phone }</td>
        <td>
         {
           handleType
            }
         </td>
        <th>
        <Button variant="outline-primary mx-2"
          onClick={handleIsShowEdit}><EditIcon style={{ color: "yellow"}} /></Button>
          {"    " }
          <Button variant="outline-primary"
            onClick={()=>{dispatch(postDelete(id))}}><DeleteIcon style={{ color: "red"}} /></Button>
          </th>
      </tr>
  );
}
