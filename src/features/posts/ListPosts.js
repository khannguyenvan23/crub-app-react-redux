import React from 'react'
import { useSelector } from 'react-redux'
import { Table } from 'react-bootstrap'
import { Post } from './Post'
export default function ListPosts(props){
    const posts = useSelector((state) =>state.posts)

    const mapListPosts = posts.map((post, index)=>(
      <Post index={index} post={post} key={"mariaOzawav" + post.id}/>
    ))
    const showAdd  = useSelector((state)=> state.showAdd.value);
    return (
        <Table striped bordered hover size="sm" variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {mapListPosts}
          {showAdd && props.children }
        </tbody>
      </Table>
    )
}