import { createSlice } from '@reduxjs/toolkit';

export const showAddReducer = createSlice({
  name: 'showAdd',
  initialState: {
      value: false
  },
  reducers: {
      isShowAdd: state => {
        state.value = !state.value
      },
  }
});


export const { isShowAdd } = showAddReducer.actions
export default showAddReducer.reducer;
