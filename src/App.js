import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import { Container } from 'react-bootstrap'
import  Header from './app/Header';
import ListPosts from './features/posts/ListPosts'
import AddPostForm from './features/posts/AddPostForm'
function App() {

  return (
    <Container>
      <Header />
      <ListPosts>
        <AddPostForm />
      </ListPosts>
    </Container>
  );
}

export default App;
