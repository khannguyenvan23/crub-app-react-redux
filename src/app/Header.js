import React from 'react';
import { Navbar, Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { isShowAdd } from '../features/show/showAddSlice'
import  AddIcon from '@material-ui/icons/Add';
export default function Headers(){
    const dispatch = useDispatch()
    return (
        <Navbar bg="dark" variant="dark"
        className="justify-content-between">
         <Navbar.Brand href="#home">
           {' '}
           <h3 className="text-primary">
             CRUD APP USING REACT HOOK REDUX
           </h3>
         </Navbar.Brand>
         <Navbar.Brand>
           {' '}
           <Button variant="outline-primary"
             onClick={()=>{
              dispatch(isShowAdd())
             }}><AddIcon style={{ color: "green",
           verticalAlign: "middle" }} /> ADD NEW</Button>
         </Navbar.Brand>
       </Navbar>
    )
}