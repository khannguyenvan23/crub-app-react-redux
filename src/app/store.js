import { configureStore } from '@reduxjs/toolkit';
import postsReducer from '../features/posts/postsSlice';
import showAddReducer from '../features/show/showAddSlice';

export default configureStore({
  reducer: {
    posts: postsReducer,
    showAdd: showAddReducer
  },
});
